--- change dates for each week ending, both in 2018 and 2019

SELECT CASE
	WHEN imprint ILIKE '%DK%' AND imprint NOT ILIKE '%bdk%' AND imprint NOT ILIKE '%EDK%' THEN 'DK'
	WHEN imprint ILIKE '%rough g%' THEN 'Rough Guides'
	WHEN imprint ILIKE '%lonely%' THEN 'Lonely Planet'
	WHEN imprint ILIKE '%steves%' THEN 'Rick Steves'
	WHEN imprint ILIKE '%fodor%' THEN 'Fodors'
	WHEN imprint ILIKE '%fromm%' THEN 'Frommers'
	WHEN imprint ILIKE '%insight g%' THEN 'Insight Guides'
	WHEN imprint ILIKE '%moon t%' THEN 'Moon Travel'
	 END AS imprint,
	SUM(CASE WHEN week_ending = '2019-02-09' THEN volume END) AS vol19,
	SUM(CASE WHEN week_ending = '2020-02-08' THEN volume END) AS vol20
FROM experiment.bookscan_us_travel_sales GROUP BY 1
END