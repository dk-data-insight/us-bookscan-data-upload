# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 12:25:43 2019

@author: awaagner
"""

import pandas as pd
import datetime as dt

filename = '//nyprhfp-1/awaagner$/BookScan/2019/Week 38/Total Market by Category Weekly_TW_201938.xlsx'

df = pd.read_excel(filename, skiprows=3)

print(df.columns)

df = df.iloc[:, [2, 3, 4]]

df = pd.melt(df, id_vars=['Category'], value_name='volume', var_name='week_ending', col_level=None)

df = df.dropna(how='any')

thisyear = filename[-11:-7]
lastyear = str(int(filename[-11:-7]) - 1)
week = filename[-7:-5]

if thisyear == '2019':
    r = thisyear + '-' + str((int(week)-1)) + '-6'
elif thisyear:
    r = thisyear + '-' + week + '-6'
    
if lastyear == '2019':
    x = lastyear + '-' + str((int(week)-1)) + '-6'
elif lastyear:
    x = lastyear + '-' + week + '-6'

df['week_ending'] = df['week_ending'].str.split('-').str[1]

df['volume'] = df['volume'].astype('int64')

df.loc[(df.week_ending.str.contains('2019', na=False)),'week_ending'] = dt.datetime.strptime(r, "%Y-%W-%w").date()

df.loc[(df.week_ending.str.contains('2018', na=False)),'week_ending'] = dt.datetime.strptime(x, "%Y-%W-%w").date()

df = df[['week_ending', 'Category', 'volume']]

df.to_csv(filename[:-5] + '_formatted.csv', index = None, header=True, encoding='UTF-8')